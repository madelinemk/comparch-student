    .set noreorder
    .data

    .text
    .globl main
    .ent main

print:
  # void print(int a)
  # {
  # // should be implemented with syscall 20
  ori $v0, $0, 20           # set syscall code to 20
  syscall                   # syscall, print will print what is in a0, which is already a, the argument to this function
  jr $ra                    # return
  nop
  # }

sum3:
  # int sum3(int a, int b, int c)
  # {

  # return a+b+c                  ;
  add $v0, $a0, $a1               #v0 = a0 + a1
  add $v0, $v0, $a2               #v0 = v0 + a2
  jr $ra                          #return
  nop

  # }

polynomial:
# int polynomial(int a, int b, int c, int d, int e)
# {

  #Push to stack: s0, s1, s2, ra
  addi $sp, $sp, -16              # decrement sp by 16
  sw $s0, 12($sp)                 # *(sp+12) = s0
  sw $s1, 8($sp)                  # *(sp+8) = s1
  sw $s2, 4($sp)                  # *(sp+4) = s2
  sw $ra, 0($sp)                  # *(sp) = ra

  # int x                       ;
    # s0 = x
  # int y
    # s1 = y
  # int z
    # s2 = z

  # x = a << b              ;
  sll $s0, $a0, $a1               # s0 = a0 << a1

  # y = c << d
  sll $s1, $a2, $a3               # s1 = a2 << a3


  # push to stack a0, a1, a2, I would do this if a0-2 needed to be
  # used again, but they are not used, so I will skip it

  # z = sum3(x, y, e)     ;
  add $a0, $0, $s0                # a0 = s0 = x
  add $a1, $0, $s1                # a1 = s1 = y
  lw $t0, 16($sp)                 # t0 = e , load from bottom of main's stack frame
  add $a2, $0, $t0                # a2 = t0
  jal sum3                        # call sum3
  nop
  add $s2, $0, $v0                # s2 = v0, z = return value

  # restore a0, a1, a2 (not actually necessary bc I never put them on the stack)

  # print(x)              ;
  add $a0, $0, $s0                # a0 = x = s0
  jal print                       # call print
  nop

  # print(y)              ;
  add $a0, $0, $s1                # a0 = y = s1
  jal print                       # call print
  nop

  # print(z)
  add $a0, $0, $s2                # a0 = z = s2
  jal print                       # call print
  nop

  # return z              ;
  add $v0, $0, $s2                # v0 = z = s2, store return value into v0
  # Restore s0, s1, s2, ra from stack
  lw $s0, 12($sp)                 # s0 = *(sp+12)
  lw $s1, 8($sp)                  # s1 = *(sp+8)
  lw $s2, 4($sp)                  # s2 = *(sp+4)
  lw $ra, 0($sp)                  # ra = *(sp)
  # Pop 4 from stack, increment sp by 16
  add $sp, $sp, 16
  jr $ra                          # return
  nop
  # }


main:
  # int main()
  # {

  #PUSH ra, s0, s1, and space for e onto the stack
  addi $sp, $sp, -16              #decrement sp by 16
  sw $ra, 12($sp)                 #store ra on stack
  sw $s0, 12($sp)                 #store s0 on stack
  sw $s1, 12($sp)                 #store s1 on stack


  # int a = 2
  ori $s0, $0, 2                  # s0 = 2

  # int f = polynomial(a, 3, 4, 5, 6) ;
  add $a0, $0, $s0                # a0 = a = s0
  ori $a1, $0, 3                  # a1 = 3
  ori $a2, $0, 4                  # a2 = 4
  ori $a3, $0, 5                  # a3 = 5

  # add 6 to bottom of main's stack frame
  ori $t0, $0, 6                  # t0 = 6
  sw $t0, 0($sp)                  # store t0 into 0(sp)
  jal polynomial                  # call polynomial
  nop
  add $s1, $0, $v0                # f = s1 = v0, return value from polynomial

  # print(a)                          ;
  add $a0, $0, $s0                # a0 = s0
  jal print                       # print
  nop

  # print(f)                          ;
  add $a0, $0, $s1                # a0 = s1
  jal print                       # print
  nop
  # }
  #

  #restore ra, s0, s1, from the stack
  lw $ra, 12($sp)                 #load ra from stack
  lw $s0, 12($sp)                 #load s0 from stack
  lw $s1, 12($sp)                 #load s1 from stack

  #POP everything off the stack pointer
  addi $sp, $sp, 16              #increment sp by 16

  ori $v0, $0, 10     # exit
  syscall
  .end main
