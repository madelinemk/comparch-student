    .set noreorder
    .data

    .text
    .globl main
    .ent main

# typedef struct elt {
# int value                   ;
# struct elt *next            ;
# } elt                       ;
# int main()
# {

main:

  # elt * head                            # s0 = head
  # elt * newelt                          # s1 = newelt

  # newelt = (elt *) MALLOC(sizeof (elt))
  ori $a0, $0, 8                          # stores sizeof(elt) into a0
  ori $v0, $0, 9                          # set syscall code to 9
  syscall                                 # MALLOC (a0)
  or $s1, $0, $v0                         # s1 = address of return from MALLOC



  # newelt->value = 1
  ori $t0, $0, 1                          # t0 = 1
  sw $t0, 0($s1)                          # store t0 into memory at s1 (newelt.value)

  # newelt->next = 0
  sw $0, 4($s1)                           # store 0 into memory at 4(s1) (newelt.next)

  # head = newelt
  or $s0, $0, $s1                         # s0 = s1

  # newelt = (elt *) MALLOC(sizeof (elt))
  ori $a0, $0, 8                          # stores sizeof(elt) into a0
  ori $v0, $0, 9                          # set syscall code to 9
  syscall                                 # MALLOC (a0)
  or $s1, $0, $v0                         # s1 = address of return from MALLOC

  # newelt->value = 2
  ori $t0, $0, 2                          # t0 = 2
  sw $t0, 0($s1)                          # store t0 into memory at s1 (newelt.value)

  # newelt->next = head
  sw $s0, 4($s1)                          # store head(s0) into 4(s1) (newelt.next)

  # head = newelt
  or $s0, $0, $s1                         # set s0 = s1, head = newelt

  # PRINT_HEX_DEC(head->value)
  lw $a0, 0($s0)                          # load 0(s0) into a0, which is 0(head)
  ori $v0, $0, 20
  syscall

  # PRINT_HEX_DEC(head->next->value)
  lw $t0, 4($s0)                          # t0 = head->next
  lw $a0, 0($t0)                          # a0 = 0(t0), a0= value of head->next
  ori $v0, $0, 20
  syscall


  # EXIT
  # }
  #

  ori $v0, $0, 10     # exit
  syscall
  .end main
