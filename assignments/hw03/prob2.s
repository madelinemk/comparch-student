    .set noreorder
    .data
#char A[] = {1,25,7,9,-1}
A: .byte 1, 25, 7, 9, -1
  .space 5

    .text
    .globl main
    .ent main

#int main()
#{
main:

  # int i
  # int current
  # int max

  # i = 0
  ori $s0, $0, 0              #s0 = i = 0

  # current = A[0]
  lui     $t0, %hi(A)         # t0 = A[i] = A[0]
  ori     $t0, $t0, %lo(A)
  lb $s1, 0($t0)              #load first byte of A into s1, s1 = current

  # max = 0
  ori $s2, $0, 0              #s2 = max = 0

  # while (current > 0) {
Loop:
  slt $t1, $0, $s1            #t1 = (0 < current)
  beq $t1, $0, Final          #branch to Final when t1 = 0, and therefore 0 < current is false
  nop
  # if (current > max)
  slt $t2, $s2, $s1           #t2 = (max < current)
  beq $t2, $0, Seq            #branch to Seq when t2 = 0, and therefore max < current is false
  nop
  # max = current
  add $s2, $0, $s1            #max = current or s2 = 0+s1

Seq:
  # i = i + 1
  addi $s0, $s0, 1            #s0 = s0 + 1
  # current = A[i]
  addi $t0, $t0, 1            #t0 = t0 + 1, increment t0 which is pointing to an address in A
  lb $s1, 0($t0)              #load A[i] into s1
  j Loop                      #go back to top of loop

  # }

Final:
  #PRINT_HEX_DEC(max)
  add $a0, $0, $s2            #set a0 to max
  ori $v0, $0, 20
  syscall

  #EXIT
  ori $v0, $0, 10     # exit
  syscall
  .end main
