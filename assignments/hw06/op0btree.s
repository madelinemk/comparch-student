	.file	1 "btree-rand.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.gnu_attribute 4, 1
	.abicalls
	.option	pic0
	.globl	root
	.section	.bss,"aw",@nobits
	.align	2
	.type	root, @object
	.size	root, 4
root:
	.space	4
	.globl	m_w
	.data
	.align	2
	.type	m_w, @object
	.size	m_w, 4
m_w:
	.word	1
	.globl	m_z
	.align	2
	.type	m_z, @object
	.size	m_z, 4
m_z:
	.word	2
	.text
	.align	2
	.globl	randInt
	.set	nomips16
	.set	nomicromips
	.ent	randInt
	.type	randInt, @function
randInt:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	sw	$fp,4($sp)
	move	$fp,$sp
	lui	$2,%hi(m_z)
	lw	$2,%lo(m_z)($2)
	nop
	andi	$4,$2,0xffff
	move	$3,$4
	sll	$2,$3,2
	move	$3,$2
	sll	$2,$3,2
	addu	$3,$3,$2
	sll	$2,$3,3
	subu	$2,$2,$3
	sll	$3,$2,5
	addu	$2,$2,$3
	addu	$2,$2,$4
	sll	$2,$2,3
	addu	$3,$2,$4
	lui	$2,%hi(m_z)
	lw	$2,%lo(m_z)($2)
	nop
	sra	$2,$2,16
	addu	$3,$3,$2
	lui	$2,%hi(m_z)
	sw	$3,%lo(m_z)($2)
	lui	$2,%hi(m_w)
	lw	$2,%lo(m_w)($2)
	nop
	andi	$2,$2,0xffff
	sll	$2,$2,4
	sll	$3,$2,2
	addu	$2,$2,$3
	sll	$3,$2,4
	subu	$3,$3,$2
	sll	$2,$3,4
	subu	$3,$2,$3
	lui	$2,%hi(m_w)
	lw	$2,%lo(m_w)($2)
	nop
	sra	$2,$2,16
	addu	$3,$3,$2
	lui	$2,%hi(m_w)
	sw	$3,%lo(m_w)($2)
	lui	$2,%hi(m_z)
	lw	$2,%lo(m_z)($2)
	nop
	sll	$3,$2,16
	lui	$2,%hi(m_w)
	lw	$2,%lo(m_w)($2)
	nop
	addu	$2,$3,$2
	move	$sp,$fp
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	randInt
	.size	randInt, .-randInt
	.align	2
	.globl	createNode
	.set	nomips16
	.set	nomicromips
	.ent	createNode
	.type	createNode, @function
createNode:
	.frame	$fp,40,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	move	$fp,$sp
	sw	$4,40($fp)
	li	$4,12			# 0xc
	jal	mips_alloc
	nop

	sw	$2,24($fp)
	lw	$3,40($fp)
	lw	$2,24($fp)
	nop
	sw	$3,0($2)
	lw	$2,24($fp)
	nop
	sw	$0,4($2)
	lw	$2,24($fp)
	nop
	sw	$0,8($2)
	lw	$2,24($fp)
	move	$sp,$fp
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	createNode
	.size	createNode, .-createNode
	.align	2
	.globl	insert
	.set	nomips16
	.set	nomicromips
	.ent	insert
	.type	insert, @function
insert:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	bne	$2,$0,$L6
	nop

	lw	$2,36($fp)
	nop
	move	$4,$2
	jal	createNode
	nop

	move	$3,$2
	lw	$2,32($fp)
	nop
	sw	$3,0($2)
	j	$L5
	nop

$L6:
	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	lw	$3,0($2)
	lw	$2,36($fp)
	nop
	slt	$2,$2,$3
	beq	$2,$0,$L8
	nop

	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	addiu	$2,$2,4
	move	$4,$2
	lw	$5,36($fp)
	jal	insert
	nop

	j	$L5
	nop

$L8:
	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	lw	$3,0($2)
	lw	$2,36($fp)
	nop
	slt	$2,$3,$2
	beq	$2,$0,$L5
	nop

	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	addiu	$2,$2,8
	move	$4,$2
	lw	$5,36($fp)
	jal	insert
	nop

$L5:
	move	$sp,$fp
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	insert
	.size	insert, .-insert
	.align	2
	.globl	inOrder
	.set	nomips16
	.set	nomicromips
	.ent	inOrder
	.type	inOrder, @function
inOrder:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	lw	$2,32($fp)
	nop
	beq	$2,$0,$L10
	nop

	lw	$2,32($fp)
	nop
	lw	$2,4($2)
	nop
	move	$4,$2
	jal	inOrder
	nop

	lw	$2,32($fp)
	nop
	lw	$2,8($2)
	nop
	move	$4,$2
	jal	inOrder
	nop

$L10:
	nop
	move	$sp,$fp
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	inOrder
	.size	inOrder, .-inOrder
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,40,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	move	$fp,$sp
	sw	$0,24($fp)
	j	$L13
	nop

$L14:
	jal	randInt
	nop

	move	$3,$2
	lui	$2,%hi(root)
	addiu	$4,$2,%lo(root)
	move	$5,$3
	jal	insert
	nop

	lw	$2,24($fp)
	nop
	addiu	$2,$2,1
	sw	$2,24($fp)
$L13:
	lw	$2,24($fp)
	nop
	slt	$2,$2,100
	bne	$2,$0,$L14
	nop

	lui	$2,%hi(root)
	lw	$2,%lo(root)($2)
	nop
	move	$4,$2
	jal	inOrder
	nop

	move	$2,$0
	move	$sp,$fp
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.ident	"GCC: (Sourcery CodeBench Lite 2015.05-18) 4.9.2"
