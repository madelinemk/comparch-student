    .set noreorder
    .data

    .text
    .globl main
    .ent main

# void print(int a)
# {
print:
  # // should be implemented with syscall 20
  ori $v0, $0, 20           # set syscall code to 20
  syscall                   # syscall, print will print what is in a0, which is already a, the argument to this function
  jr $ra                    # return
  nop

# int main()
# {
main:

  #PUSH space onto stack for ra s0, s1, and A[8]
  addi $sp, $sp, -44        #decrement sp by 44
  sw $ra, 40($sp)           # store ra on stack
  sw $s0, 36($sp)           # store s0 on stack
  sw $s1, 32($sp)           # store s1 on stack

  # int A[8]
  or $s0, $0, $sp           # s0 = base address of A, stack pointer

  # int i                   # s1 = i
  # A[0] = 0
  sw $0, 0($s0)             # store 0 into 0 offset of s0 (A[0])

  # A[1] = 1
  ori $t0, $0, 1            # t0 = 1
  sw $t0, 4($s0)            # store t0 into 4 offset from s0, which is the base address of A

  # for (i = 2; i < 8; i++) {
  ori $s1, $0, 2            #set i to 2, s1 = i = 2
Loop:
  slti $t1, $s1, 8          #t1 = s1 < 8, t1 = (i < 8)
  beq $t1, 0, Final         #branch to final when t1 = 0, meaning when i < 8 is false
  nop

  # A[i] = A[i-1] + A[i-2]
  sll $t3, $s1, 2           #t3 = offset from base address of A that the i-th element is.
  add $t2, $s0, $t3         #t2 = address of A[i] = baseA + offset = s0 + t3

  lw $t4, -4($t2)           #load A[i-1] into t4
  lw $t5, -8($t2)           #load A[i-2] into t5
  add $t6, $t5, $t4         #t6 = t4 + t5
  sw $t6, 0($t2)            #store t6 into A[i]

  # print(A[i])
  or $a0, $0, $t6
  jal print
  nop

  addi $s1, $s1, 1          #increment s1 which represents i
  j Loop                    #go to the top of the loop
  nop


  # }
  # }

Final:

  #restore ra, s0, and s1 from the stack
  lw $ra, 40($sp)           # load ra from stack
  lw $s0, 36($sp)           # load s0 from stack
  lw $s1, 32($sp)           # load s1 from stack

  #POP everything from this function off the stack pointer
  addi $sp, $sp, 44         #increment sp by 44

  ori $v0, $0, 10     # exit
  syscall
  .end main
