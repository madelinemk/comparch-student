    .set noreorder
    .data

    .text
    .globl main
    .ent main

  #typedef struct record {
  #int field0
  #int field1
  #} record

  #int main()
  #{

main:

  #record *r = (record *) MALLOC(sizeof(record))
  ori $a0, $0, 8                                  # stores sizeof(record) into a0
  ori $v0, $0, 9                                  # set syscall code to 9
  syscall                                         # MALLOC (a0)

  or $t0, $0, $v0                                 # t0 = address of block allocated for r

  #r->field0 = 100
  ori $t1, $0, 100                                # t1 = 100
  sw $t1, 0($t0)                                  # store t1 into memory at t0

  #r->field1 = -1
  addi $t1, $0, -1                                 # t1 = -1
  sw $t1, 4($t0)                                  # store t1 into memroy at t0 + 4

  #EXIT
  #}

  ori $v0, $0, 10     # exit
  syscall
  .end main
