    .set noreorder
    .data

#int A[8]
A:   .word
.space 32

    .text
    .globl main
    .ent main
main:

  # int i

  lui $t0, %hi(A)             #t0 = A, base address of A
  ori $t0, $t0, %lo(A)

  # A[0] = 0
  sw $0, 0($t0)
  # A[1] = 1
  addi $t1, $0, 1             #t1 = 1
  sw $t1, 4($t0)              #store t1 in A[1]



  # for (i = 2; i < 8; i++) {
  ori $s0, $0, 2            #set i to 2, s0 = i = 2
Loop:
  slti $t1, $s0, 8          #t1 = s0 < 8, t1 = (i < 8)
  beq $t1, 0, Final         #branch to final when t1 = 0, meaning when i < 8 is false
  nop

  # A[i] = A[i-1] + A[i-2]
  sll $t3, $s0, 2           #t3 = offset from base address of A that the i-th element is.
  add $t2, $t0, $t3         #t2 = address of A[i]

  lw $t4, -4($t2)           #load A[i-1] into t4
  lw $t5, -8($t2)           #load A[i-2] into t5
  add $t6, $t5, $t4         #t6 = t4 + t5
  sw $t6, 0($t2)            #store t6 into A[i]

  # PRINT_HEX_DEC(A[i])
  lw $a0, 0($t2)            #set a0 to A[i]
  ori $v0, $0, 20
  syscall

  addi $s0, $s0, 1          #increment s0 which represents i
  j Loop                    #go to the top of the loop
  nop
  # }

Final:
#  EXIT                         ;

    ori $v0, $0, 10     # exit
    syscall
    .end main
