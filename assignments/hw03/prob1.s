    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
  ori $s0, $0, 84   #int score = 84
        #int grade

  #if (score >= 90)
  slti $t0, $s0, 90 #t0 = score < 90
  bne $t0, $0, seq1 #branch on t0 not equal 0
  nop     #nop

  #grade = 4
  ori $s1, $0, 4    #s1 = 4

  j final     #jump to end

seq1:
  #else if (score >= 80)
  slti $t0, $s0, 80 #t0 = score < 80
  bne $t0, $0, seq2 #branch to seq2 on t0 not equal 0
  nop     #nop

  #grade = 3
  ori $s1, $0, 3    #s1 = 3

  j final     #jump to end

seq2:
  #else if (score >= 70)
  slti $t0, $s0, 70 #t0 = score < 70
  bne $t0, $0, seq3 #branch to seq3 on t0 not equal 0
  nop     #nop

  #grade = 2
  ori $s1, $0, 2    #s1 = 2
  j final     #jump to end
seq3:
  #else
  #grade = 0
  ori $s1, $0, 0    #s1 = 0

final:
  #PRINT_HEX_DEC(grade)
  or  $a0, $0, $s1  #set a0 to grade: a0 = s1
  ori $v0, $0, 20   #set syscall code: v0=20
  syscall

  #EXIT
        ori $v0, $0, 10       #set syscall code: v0=10
        syscall
    .end main
