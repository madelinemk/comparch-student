	.file	1 "btree-rand.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.gnu_attribute 4, 1
	.abicalls
	.option	pic0
	.text
	.align	2
	.globl	randInt
	.set	nomips16
	.set	nomicromips
	.ent	randInt
	.type	randInt, @function
randInt:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$9,%hi(m_z)
	lw	$7,%lo(m_z)($9)
	lui	$8,%hi(m_w)
	andi	$3,$7,0xffff
	sll	$2,$3,2
	sll	$4,$3,4
	lw	$6,%lo(m_w)($8)
	addu	$4,$2,$4
	sll	$5,$4,3
	subu	$4,$5,$4
	andi	$2,$6,0xffff
	sll	$5,$2,4
	sll	$10,$4,5
	sll	$2,$2,6
	addu	$2,$5,$2
	addu	$4,$4,$10
	addu	$4,$4,$3
	sll	$5,$2,4
	subu	$2,$5,$2
	sll	$4,$4,3
	addu	$4,$4,$3
	sll	$5,$2,4
	sra	$3,$7,16
	addu	$3,$4,$3
	subu	$4,$5,$2
	sra	$2,$6,16
	addu	$2,$4,$2
	sll	$4,$3,16
	sw	$2,%lo(m_w)($8)
	sw	$3,%lo(m_z)($9)
	j	$31
	addu	$2,$2,$4

	.set	macro
	.set	reorder
	.end	randInt
	.size	randInt, .-randInt
	.align	2
	.globl	createNode
	.set	nomips16
	.set	nomicromips
	.ent	createNode
	.type	createNode, @function
createNode:
	.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$16,24($sp)
	move	$16,$4
	sw	$31,28($sp)
	jal	mips_alloc
	li	$4,12			# 0xc

	lw	$31,28($sp)
	sw	$16,0($2)
	sw	$0,4($2)
	lw	$16,24($sp)
	sw	$0,8($2)
	j	$31
	addiu	$sp,$sp,32

	.set	macro
	.set	reorder
	.end	createNode
	.size	createNode, .-createNode
	.align	2
	.globl	insert
	.set	nomips16
	.set	nomicromips
	.ent	insert
	.type	insert, @function
insert:
	.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$17,32($sp)
	sw	$16,28($sp)
	sw	$31,36($sp)
	move	$16,$4
	move	$17,$5
$L5:
	lw	$2,0($16)
	nop
	beq	$2,$0,$L12
	nop

$L6:
	lw	$3,0($2)
	addiu	$16,$2,4
	slt	$4,$17,$3
	bne	$4,$0,$L5
	slt	$3,$3,$17

	beq	$3,$0,$L4
	addiu	$16,$2,8

	lw	$2,0($16)
	nop
	bne	$2,$0,$L6
	nop

$L12:
	jal	mips_alloc
	li	$4,12			# 0xc

	sw	$17,0($2)
	sw	$0,4($2)
	sw	$0,8($2)
	sw	$2,0($16)
$L4:
	lw	$31,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	insert
	.size	insert, .-insert
	.align	2
	.globl	inOrder
	.set	nomips16
	.set	nomicromips
	.ent	inOrder
	.type	inOrder, @function
inOrder:
	.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$16,24($sp)
	sw	$31,28($sp)
	beq	$4,$0,$L13
	move	$16,$4

$L15:
	lw	$4,4($16)
	jal	inOrder
	nop

	lw	$16,8($16)
	nop
	bne	$16,$0,$L15
	nop

$L13:
	lw	$31,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,32

	.set	macro
	.set	reorder
	.end	inOrder
	.size	inOrder, .-inOrder
	.section	.text.startup,"ax",@progbits
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$17,32($sp)
	lui	$17,%hi(root)
	sw	$16,28($sp)
	sw	$31,36($sp)
	li	$16,100			# 0x64
	addiu	$17,$17,%lo(root)
$L22:
	jal	randInt
	addiu	$16,$16,-1

	move	$5,$2
	jal	insert
	move	$4,$17

	bne	$16,$0,$L22
	lui	$2,%hi(root)

	lw	$16,%lo(root)($2)
	nop
	beq	$16,$0,$L23
	nop

$L24:
	lw	$4,4($16)
	jal	inOrder
	nop

	lw	$16,8($16)
	nop
	bne	$16,$0,$L24
	nop

$L23:
	lw	$31,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	move	$2,$0
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.globl	m_z
	.data
	.align	2
	.type	m_z, @object
	.size	m_z, 4
m_z:
	.word	2
	.globl	m_w
	.align	2
	.type	m_w, @object
	.size	m_w, 4
m_w:
	.word	1
	.globl	root
	.section	.bss,"aw",@nobits
	.align	2
	.type	root, @object
	.size	root, 4
root:
	.space	4
	.ident	"GCC: (Sourcery CodeBench Lite 2015.05-18) 4.9.2"
